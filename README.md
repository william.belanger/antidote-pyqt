# PyQt Interface for Druide Antidote
Cross-platform PyQt5 interface for Antidote 9, 10, 11

## Usage
Import the `antidote` module
```
import antidote
```

The handler class then take care of the local server initialization, using either the DBus protocol for Linux or the COM protocol for Windows. The COM server registration is achieved in the user space and thus requires no privilege elevation. The `getHandler()` function return the appropriate handler object for the user OS, where the DBus API allows multiple concurrent instances. If no Antidote installation is found, a `FileNotFoundError` is raised.
```
try:
    self.antidote = antidote.getHandler("MyProject", self.textWidget)
    self.antidote.init()
except FileNotFoundError:
    print("Could not find Antidote binary")
```

The error can be silenced with the keyword argument `silent`. If Antidote binary cannot be found, it will return `None`.
```
self.antidote = antidote.getHandler("MyProject", self.textWidget, silent=True)
if self.antidote:
    self.antidote.init()
```

Once `init()` method is called, the handler responds with a `ready` signal. Then the tools are accessed using `corrector`, `dictionnary` and `guide` methods.
```
self.antidote.corrector()
self.antidote.dictionnary()
self.antidote.guide()
```

Spefific dictionnaries or guide are called by passing an argument, as in `self.antidote.dictionnary("D_Wikipedia")`. By default, `D_Defaut` and `G_Defaut` are used. Accepted values for `dictionnary()` are:

```D, D_Defnitions, D_Synonymes, D_Antonymes, D_Cooccurrences, D_hampLexical, D_Conjugaison, D_Famille, D_Citations, D_Historique, D_Illustrations, D_Wikipedia```

And for `guide()`:

```G, G_Orthographe, G_Lexique, G_Grammaire, G_Syntaxe, G_Ponctuation, G_Style, G_Redaction, G_Typographie, G_Phonetique, G_Historique, G_PointsDeLangue```

Multiple antidote instance can be edited in parallel. For the DBus protocol, this requires one handler per widget. By default, the document name correspond to the widget `objectName()`.

## Project integration
Dependencies are `PyQt5` and `pywin32` (Windows). If the current name space of the COM class is changed (antidote.com.Adapter), the `namespace` variable must be updated accordingly in method `register` of `Adapter` class in `antidote/com.py`.
