#!/usr/bin/python3
import sys
import antidote
from PyQt5 import QtGui, QtWidgets

ID = "MyProject"


class MainWindow(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        self.textEdit_1 = QtWidgets.QTextEdit("First text to corrrect.")
        self.textEdit_2 = QtWidgets.QTextEdit("Second texte to corect.")
        self.textEdit_1.setObjectName("Doc-1")
        self.textEdit_2.setObjectName("Doc-2")

        self.correctButton_1 = QtWidgets.QPushButton("Correct")
        self.correctButton_2 = QtWidgets.QPushButton("Correct")
        self.dictButton_1 = QtWidgets.QPushButton("Dictionnary")
        self.dictButton_2 = QtWidgets.QPushButton("Dictionnary")
        self.guideButton = QtWidgets.QPushButton("Guide")

        self._setupLayout()
        self._setupAntidote()

    def closeEvent(self, event: QtGui.QCloseEvent):
        super().closeEvent(event)
        sys.exit(0)  # Kills the hanging COM server in QThread

    def _setupAntidote(self):
        try:
            self.handler_1 = antidote.getHandler(ID, self.textEdit_1)
            self.correctButton_1.clicked.connect(self.handler_1.corrector)
            self.dictButton_1.clicked.connect(self.handler_1.dictionnary)
            self.guideButton.clicked.connect(self.handler_1.guide)
            self.handler_1.init()

            self.handler_2 = antidote.getHandler(ID, self.textEdit_2)
            self.correctButton_2.clicked.connect(self.handler_2.corrector)
            self.dictButton_2.clicked.connect(self.handler_2.dictionnary)
            self.handler_2.init()
        except FileNotFoundError:
            print("Could not find Antidote binary")

    def _setupLayout(self):
        groupBox_1 = QtWidgets.QGroupBox("Document 1")
        boxLayout_1 = QtWidgets.QVBoxLayout(groupBox_1)
        boxLayout_1.addWidget(self.textEdit_1)
        boxLayout_1.addWidget(self.correctButton_1)
        boxLayout_1.addWidget(self.dictButton_1)
        boxLayout_1.addWidget(self.guideButton)

        groupBox_2 = QtWidgets.QGroupBox("Document 2")
        boxLayout_2 = QtWidgets.QVBoxLayout(groupBox_2)
        boxLayout_2.addWidget(self.textEdit_2)
        boxLayout_2.addWidget(self.correctButton_2)
        boxLayout_2.addWidget(self.dictButton_2)

        layout = QtWidgets.QHBoxLayout(self)
        layout.addWidget(groupBox_1)
        layout.addWidget(groupBox_2)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = MainWindow()
    gui.show()
    app.exec()
