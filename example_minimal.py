#!/usr/bin/python3
import sys
import antidote
from PyQt5 import QtGui, QtWidgets

ID = "MyProject"


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.body = QtWidgets.QTextEdit("Texte à corrigée.")
        self.body.setObjectName("My Document")
        self.setCentralWidget(self.body)
        try:
            self.antidote = antidote.getHandler(ID, self.body)
            self.antidote.ready.connect(self.antidote.corrector)
            self.antidote.init()
        except FileNotFoundError:
            print("Could not find Antidote binary")

    def closeEvent(self, event: QtGui.QCloseEvent):
        super().closeEvent(event)
        sys.exit(0)  # Kills the hanging COM server in QThread


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    gui = MainWindow()
    gui.show()
    app.exec()
